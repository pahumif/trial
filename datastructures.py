x = [10, 20, 30] # list
#    0   1   2

y = (10, 20, 30) # tuple

print(y[-1])

# add, remove, modify
x.append([200,400])
x.extend([500,1000])


z = x.pop()
print(z)

x.remove(20)
print(x)
print(len(x))

z = {10: 'hello', 20: 'goodbye'}
z['name'] = 'fjorela'
z[20] = 'hi'
print(z)